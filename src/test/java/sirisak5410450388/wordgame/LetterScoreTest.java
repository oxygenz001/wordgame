package sirisak5410450388.wordgame;

import static org.junit.Assert.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class LetterScoreTest {
	
	private LetterScoreList score;
	
	@Before
	public void setUp() throws Exception {
		score = new LetterScoreList(new StubDataMenager());
	}
	
	@Test
	public void testWordCat(){
	//c = 3, a = 2, t = 1 ; 3+2+1 = 6
	assertEquals(score.getWordScores("cat"), 6);
	}
	@Test
	public void testWordDog(){
	//d = 2, o = 3, g = 4 ; 2+3+4 = 9
	assertEquals(score.getWordScores("dog"), 9);
	}
	
	//Inner Class
	private class StubDataMenager implements IDataManager{

		public Map<Character, Integer> getCharacterScoreMap() {
			Map<Character,Integer> IDataMap = new HashMap<Character,Integer>();
			IDataMap.put('c', 3);
			IDataMap.put('a', 2);
			IDataMap.put('t', 1);
			IDataMap.put('d', 2);
			IDataMap.put('o', 3);
			IDataMap.put('g', 4);
			return IDataMap;
		}

		public List<String> getWordList() {
			// TODO Auto-generated method stub
			return null;
		}

		
	}
}
