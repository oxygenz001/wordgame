package sirisak5410450388.wordgame;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Just like WordListTest, but I use jMock to help create IDataManager stubs.
 * (There is no interaction testing in this test.)
 * 
 * @author Usa Sammpun
 *
 */
@RunWith(JMock.class)
public class WordListTestWithJMock {

	private Mockery context = new JUnit4Mockery();
	private final IDataManager mockMgr = context.mock(IDataManager.class);
	private List<String> words = new ArrayList<String>();
	private WordList list;
	
	@Before
	public void setup() {
		
		context.checking(new Expectations() {{
			allowing (mockMgr).getWordList();
			will(returnValue(words));
		}});
		
		list = new WordList(mockMgr);
		words.add("weed");
		
		
	}

	@Test
	public void testContainsWordTrueUsingMockFramework() {
		boolean result = list.containsWord("weed");
		assertTrue(result);
	}
	
	@Test
	public void testContainsWordFalseUsingMockFramework() {
		boolean result = list.containsWord("xyz");
		assertFalse(result);
	}
	
}
