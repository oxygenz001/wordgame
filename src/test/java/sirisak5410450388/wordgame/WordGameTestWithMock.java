package sirisak5410450388.wordgame;

import static org.junit.Assert.*;



import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;






@RunWith(JMock.class)
public class WordGameTestWithMock {

	private Mockery context = new JUnit4Mockery() {{
		setImposteriser(ClassImposteriser.INSTANCE);
	}};

	private final LetterScoreList mockLetter = context.mock(LetterScoreList.class);
	private final Player mockPlayer = context.mock(Player.class);
	private final Hand mockHand = context.mock(Hand.class);
	private final WordList mockList = context.mock(WordList.class);
	private int score;
	private String word;

	private WordGame wordGame;

	
	@Before
	public void setup() {

		wordGame = new WordGame(8, mockList, mockLetter, mockPlayer);
		score = 10;
		word = "cat";
		
		context.checking(new Expectations() {{
			
			
			
			allowing(mockHand).containsLetters(word);
			will(returnValue(true));
			
			allowing(mockHand).isEmpty();
			will(returnValue(false));
			
			allowing(mockList).containsWord(word);
			will(returnValue(true));		
		
			allowing(mockLetter).getWordScores(word);
			will(returnValue(score));
			
			allowing(mockPlayer).getPoint();
			will(returnValue(score));
			
			atLeast(1).of(mockHand).update(word);
			
			atLeast(1).of(mockPlayer).addPoint(score);
			
			
			
		}});
		
		
		
	}

	@Test
	public void testA() {
		wordGame.tryWord(word);
		assertEquals(mockPlayer.getPoint(), 10);
	}
	

	
	
}
