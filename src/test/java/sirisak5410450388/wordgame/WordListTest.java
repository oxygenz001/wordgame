package sirisak5410450388.wordgame;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class WordListTest {

	private WordList list;

	@Before
	public void setUp() throws Exception {
		list = new WordList(new StubDataMenager());   
	}

	@Test
	public void testContainsWordTrue() {
		boolean reesult = list.containsWord("cat");
		assertTrue(reesult);
	}
	
	@Test
	public void testContainsWordFalse(){
		boolean result = list.containsWord("dog");
		assertFalse(result);
	}
	
	// สร้าง Inner Class
	private class StubDataMenager implements IDataManager{

		public Map<Character, Integer> getCharacterScoreMap() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public List<String> getWordList() { // ต้อง return list ออกมาให้คุณ
			ArrayList<String> list = new ArrayList<String>();
			list.add("cat");
			return list;
		}
		
	}
	
}
