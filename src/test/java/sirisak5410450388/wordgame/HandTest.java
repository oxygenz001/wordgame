package sirisak5410450388.wordgame;

import static org.junit.Assert.*;



import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class HandTest {

	private Map<Character,Integer> handFreq;
	private Hand hand;
	
	@Before
	public void setup() {
		handFreq = new HashMap<Character,Integer>();
		hand = new Hand();
		
		hand.addLetter('r');
		hand.addLetter('a');
		hand.addLetter('a');
		hand.addLetter('t');
		hand.addLetter('t');
		hand.addLetter('t');
		hand.addLetter('c');
		
		handFreq.put('r', 1);
		handFreq.put('a', 2);
		handFreq.put('t', 3);
		handFreq.put('c', 1);
		//word = ( r a a t t t c )
	}

	@Test
	public void test() {
		boolean result1 = hand.containsLetters("cat");
		assertTrue(result1);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testHandThrowsExceptionEmptyWord() {
		hand.containsLetters("");
	}
	@Test(expected=IllegalArgumentException.class)
	public void testHandThrowsExceptionNullWord() {
		hand.containsLetters(null);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testHandThrowsExceptionNotCreatWord() {
		 hand.update("racg");
	}
	@Test
	public void testCheckWordOnHand(){
		//word1 = ( r a a t t t c )
		hand.update("attt");
		//word1 = ( r  a  c )
		Map<Character,Integer> handFreqMapTest = new HashMap<Character,Integer>();
		handFreqMapTest.put('r', 1);
		handFreqMapTest.put('a', 1);
		handFreqMapTest.put('c', 1);
		//word2 = ( r  a  c )
		assertTrue(handFreqMapTest.equals(hand.handFreq));
		
		
	}
	
	@Test
	public void testIsEmptry(){
		assertFalse(hand.isEmpty());
	}

	@After
	@Test(timeout=5)
	public void testHandTimeout() {

		for (int i = 0; i < 9999; i++) {
			hand.containsLetters("rac");
		}
	}

	
	
	 

}

