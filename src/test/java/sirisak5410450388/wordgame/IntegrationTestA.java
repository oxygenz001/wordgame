package sirisak5410450388.wordgame;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;



public class IntegrationTestA {

	private Player player;
	private Hand hand;
	private WordList list;
	private FileDataManager file;
	private String words;
	private LetterScoreList scoreList;
	private WordGame wordGame;
	private int scoreSize;
	private Map<Character,Integer> handFreq;
	
	@Before
	public void setUp(){
		scoreSize = 8;
		words = "cat";
		
		file = new FileDataManager();
		player = new Player(hand);
		list = new WordList(file);
		scoreList = new LetterScoreList(file);
		handFreq = file.getCharacterScoreMap();
		hand = new Hand();
	
		wordGame = new WordGame(scoreSize, list, scoreList, player);
		wordGame.giveHand(hand);
		
		hand.addLetter('a');
		hand.addLetter('a');
		hand.addLetter('t');
		hand.addLetter('t');
		hand.addLetter('t');
		hand.addLetter('c');
		hand.addLetter('m');
		hand.addLetter('n');
		
		
	}

	@Test
	public void test1HandAndPlayer() {
		player.addPoint(10);
		assertEquals(10, player.getPoint());
	}
	
	@Test
	public void test2WordListAndFileSystem() {
		assertTrue(list.containsWord(words));	
	}

	@Test
	public void test3LetterScoreListAndFileSystem() {
		assertEquals(scoreList.getWordScores(words), 6);	
	}
	
	@Test
	public void test4WordGamePlayerAndHand() {
		assertEquals(wordGame.tryWord(words), 6);
	}
	
	@Test
	public void test5WordGamePlayerHandWordListAndFileDataManager() {
		wordGame.tryWord(words);
		assertEquals(wordGame.getPlayerPoint(), 6);
	
		
		
	}
	
}
