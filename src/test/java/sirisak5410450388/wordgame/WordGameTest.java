package sirisak5410450388.wordgame;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;


public class WordGameTest {

	private Map<Character,Integer> handFreq;
	private HandStub handStub;
	
	private WordGame wordGame;
	private WordListStub wordListStub;
	private LetterScoreListStub letterScoreListStub;
	private IDataManagerStub stubDataManager;
	private PlayerStub playerStub;
	private int HandSize;

	@Before
	public void setup() {
		
		handFreq = new HashMap<Character,Integer>();
		handStub = new HandStub();
		playerStub = new PlayerStub(handStub);
		stubDataManager = new IDataManagerStub();
		wordListStub = new WordListStub(stubDataManager);
		letterScoreListStub = new LetterScoreListStub(stubDataManager);
		HandSize = handFreq.size();
		wordGame = new WordGame(HandSize, wordListStub,letterScoreListStub,playerStub);
		wordGame.giveHand(handStub);

	}

	@Test
	public void testWordCat() {
				
		handStub.addLetter('r');
		handStub.addLetter('a');
		handStub.addLetter('a');
		handStub.addLetter('t');
		handStub.addLetter('t');
		handStub.addLetter('t');
		handStub.addLetter('c');

		boolean reesult = wordListStub.containsWord("cat");
		assertTrue(reesult);
		assertEquals(wordGame.tryWord("cat"), 6);
		assertEquals(wordGame.getPlayerPoint(), 6);
		
		
	}
	
	@Test
	public void testWordRatAndCat() {
		
		handStub.addLetter('r');
		handStub.addLetter('a');
		handStub.addLetter('a');
		handStub.addLetter('t');
		handStub.addLetter('t');
		handStub.addLetter('t');
		handStub.addLetter('c');

		boolean reesult1 = wordListStub.containsWord("rat");
		assertTrue(reesult1);
		assertEquals(wordGame.tryWord("rat"), 7);
		assertEquals(wordGame.getPlayerPoint(), 7);
		boolean reesult2 = wordListStub.containsWord("cat");
		assertTrue(reesult2);
		assertEquals(wordGame.tryWord("cat"), 6);
		assertEquals(wordGame.getPlayerPoint(), 13);
		
	}
	
	@Test
	public void testWordDog() {
		
		handFreq.put('r', 1);
		handFreq.put('a', 2);
		handFreq.put('t', 3);
		handFreq.put('c', 1);

		boolean reesult = wordListStub.containsWord("dog");
		assertFalse(reesult);
		assertEquals(wordGame.tryWord("dog"), 0);
		assertEquals(wordGame.getPlayerPoint(), 0);
		
	}

	

	//Inner class
	private class LetterScoreListStub extends LetterScoreList{

		public LetterScoreListStub(IDataManager mgr) {
			super(mgr);
			// TODO Auto-generated constructor stub
		}

	}

	//Inner class 
	private class WordListStub extends WordList{

		public WordListStub(IDataManager mgr) {
			super(mgr);
			// TODO Auto-generated constructor stub
		}



	}

	private class PlayerStub extends Player{

		public PlayerStub(Hand hand) {
			super(hand);
			// TODO Auto-generated constructor stub
		}

	} 
//////////////////////////////////////////////////////////////////////////////////
	//Inner class
	private class HandStub extends Hand{
		
		public HandStub() {
		super();
		
		
	}

	}
/////////////////////////////////////////////////////////////////////////////////

	// Inner class
	private class IDataManagerStub implements IDataManager{

		public Map<Character, Integer> getCharacterScoreMap() {
			Map<Character,Integer> IDataMap = new HashMap<Character,Integer>();
			IDataMap.put('c', 3);
			IDataMap.put('a', 2);
			IDataMap.put('t', 1);
			IDataMap.put('r', 4);
			IDataMap.put('d', 2);
			IDataMap.put('o', 4);
			IDataMap.put('g', 2);

			return IDataMap;
		}

		public List<String> getWordList() {
			ArrayList<String> list = new ArrayList<String>();
			list.add("cat");
			list.add("rat");
			list.add("car");
			return list;
		}

	}

}


