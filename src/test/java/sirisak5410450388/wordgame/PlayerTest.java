package sirisak5410450388.wordgame;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class PlayerTest {

	private Player player;
	private HandStub handStub;
	
	@Before
	public void setUp(){
		handStub = new HandStub();
		player = new Player(new HandStub());
	}

	@Test
	public void testAddPoint() {
		player.addPoint(10);
		assertEquals(10, player.getPoint());
	}
	@Test
	public void testAddPoint2() {
		player.addPoint(10);
		player.addPoint(20);
		assertEquals(30, player.getPoint());
	}

	private class HandStub extends Hand{
		
		public HandStub() {
			super();
		}
		
		@Override
		public boolean containsLetters(String word){
			return true;
		}
		
		@Override
		public void update(String word){
		}
		
		@Override
		public boolean isEmpty(){
			return true;
		}
		
	}

}
