package sirisak5410450388.wordgame;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * เป็นคลาสที่ดึงข้อมูลเกมจากไฟล์
 * @author Usa Sammapun
 *
 */
public class FileDataManager implements IDataManager {

	private Map<Character, Integer> characterScoreMap = null;
	private List<String> wordList = null;
	private final String scoreFileName = "CharacterScoreMap.txt";
	private final String wordFileName = "WordList.txt";
	
	public Map<Character, Integer> getCharacterScoreMap() {
		
		if (characterScoreMap != null)
			return characterScoreMap;
		
		characterScoreMap = new HashMap<Character, Integer>();
		
		try {
			FileReader file = new FileReader(scoreFileName);
			BufferedReader in = new BufferedReader(file);

			for (String line = in.readLine(); line != null; line = in.readLine()) {
				StringTokenizer data = new StringTokenizer(line, ",");
				char character = data.nextToken().charAt(0);
				int score = Integer.parseInt(data.nextToken());
				characterScoreMap.put(character, score);
			}

		} catch(FileNotFoundException e) {
			System.err.println("Character score data file name is incorrect");
			System.exit(1);
		} catch(IOException e) {
			System.err.println("Problems reading character score data file");
			System.exit(1);
		} 
		
		return characterScoreMap;
	}

	public List<String> getWordList() {
		if (wordList != null)
			return wordList;
		
		wordList = new ArrayList<String>();
		
		try {
			FileReader file = new FileReader(wordFileName);
			BufferedReader in = new BufferedReader(file);

			for (String line = in.readLine(); line != null; line = in.readLine()) {
				wordList.add(line.trim());
			}

		} catch(FileNotFoundException e) {
			System.err.println("Word list data file name is incorrect");
			System.exit(1);
		} catch(IOException e) {
			System.err.println("Problems reading word list data file");
			System.exit(1);
		} 
		return wordList;
	}

}
