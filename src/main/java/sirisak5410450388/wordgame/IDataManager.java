package sirisak5410450388.wordgame;

import java.util.List;
import java.util.Map;

public interface IDataManager {

	
	Map<Character , Integer> getCharacterScoreMap();
	List<String> getWordList();
	
}
