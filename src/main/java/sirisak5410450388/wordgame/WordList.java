package sirisak5410450388.wordgame;

import java.util.List;

public class WordList {

	private IDataManager manager;
	private List<String> list;
	
	public WordList(IDataManager mgr) {
		manager = mgr;
		list = manager.getWordList();
	}
	
	public boolean containsWord(String word){	
		return list.contains(word);		
	}
}
