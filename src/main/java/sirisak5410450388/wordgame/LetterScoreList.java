package sirisak5410450388.wordgame;

import java.util.Map;


public class LetterScoreList {
private Map<Character,Integer> map;
private IDataManager manager;


public LetterScoreList(IDataManager mgr) {
	this.manager = mgr;
	map = manager.getCharacterScoreMap();

}

public int getWordScores(String word){
	int score = 0;
	char c;

	for(int i = 0;i < word.length();i++){
		c = word.charAt(i);
		score  += map.get(c);
	}
	
	
	return score;
	
}

}
