package sirisak5410450388.wordgame;

public class Player {
	
	private Hand playerHand;
	int playerScore;
	
	public Player(Hand hand) {
		this.playerHand = hand;
	}
	
	public Hand getHand(){
		return playerHand;
	}
	
	public void addPoint(int points){
		playerScore += points;
	}
	public int getPoint(){
		return playerScore;
	}
	
}
