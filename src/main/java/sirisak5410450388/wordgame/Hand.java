package sirisak5410450388.wordgame;

import java.util.HashMap;
import java.util.Map;


public class Hand {

	public Map<Character,Integer> handFreq = new HashMap<Character,Integer>();
/*
	public Hand(Map<Character,Integer> initialHand) {
		this.handFreq = initialHand;	
	}
*/
	public Hand(){
	
	}
	
	public void addLetter(char letter){
		if(handFreq.get(letter) == null){
		handFreq.put(letter, 1);
		}
		else{
		int a = handFreq.get(letter);
		a += 1;
		handFreq.put(letter, a);
		}
		
	}
	
	public int getSize(){
		return handFreq.size();
	}

	public boolean containsLetters(String word){
		if(word == "" || word == null){
			throw new IllegalArgumentException("No word");
		}
		boolean check = true;
		for(int i = 0; i < word.length();i++){
			char c = (word.substring(i, i+1).charAt(0));
			if(handFreq.get(c) == null){
				check = false;
				break;
			}
		}
		return check;
	}

	public void update(String word){
		for(int i = 0; i < word.length();i++){
			char c = (word.substring(i, i+1).charAt(0));
			if(handFreq.get(c) == null){
				throw new IllegalArgumentException("No word no");
			}
			else{
				handFreq.put(c, (handFreq.get(c))-1);
				if(handFreq.get(c) == 0){
					handFreq.remove(c);
				}
			}
		}
	}
	public boolean isEmpty(){
		return handFreq.isEmpty();
	}
	
	protected Map<Character,Integer> checkWordOnHand(){
		
		return handFreq;
	}
}