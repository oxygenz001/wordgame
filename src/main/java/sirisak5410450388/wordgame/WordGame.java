package sirisak5410450388.wordgame;

public class WordGame {

	private WordList wordList;
	private LetterScoreList score;
	private Player player;
	private Hand hand;
	private int handSize;
	
	
	public WordGame(int number,WordList wordList,LetterScoreList score,Player player) {
		
		this.handSize = number;
		this.wordList = wordList;
		this.score = score;
		this.player = player;
		
	}

	public void giveHand(Hand hand){	
		this.hand = hand;
	}

	public int tryWord(String word) {
		
		int score = 0;
		
		if(wordList.containsWord(word)){
			if(hand.containsLetters(word)){
				score = this.score.getWordScores(word);
			
				
				hand.update(word);
				if(hand.isEmpty()){
					score += 50;
					
				}
			}
		}
		
		player.addPoint(score);
		return score;
	}
	
	public int getPlayerPoint() {
		
		return player.playerScore;
	}
	
	public Hand getPlayerHand() {
		return hand;
	}
}
