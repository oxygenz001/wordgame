package sirisak5410450388.wordgame;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class WordGameGUI {

	private JFrame chooseSizeFrame, wordGameFrame;
	
	private JLabel chooseLabel, playLabel, totalScoreLabel;
	private JTextField sizeField, wordField;
	private JButton submitBtn, playBtn, newHandBtn;
	private JTextArea resultView;
	private Hand hand;
	
	private WordGame game;
	private IDataManager data;
	private int size;
	
	public WordGameGUI() {
		
		data = new FileDataManager();
		
		chooseSizeFrame = new JFrame();
		chooseSizeFrame.setTitle("Choose Hand Size");
		wordGameFrame = new JFrame();
		wordGameFrame.setTitle("Word Game");
		
		createChooseSizeFrame();
		createWordGameFrame();


	}
	
	private void newGame() {

		//hand = generateRandomHand(size);
		generateRandomHand(size);////////////////////////////////////////////////////////////////////
		Player player = new Player(hand);
		game = new WordGame(size, new WordList(data), new LetterScoreList(data), player);
		game.giveHand(hand);
		resultView.setText("Current Hand: "+game.getPlayerHand());
		
	}
	
	private void createWordGameFrame() {
		wordGameFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		playLabel = new JLabel("Enter word: ");
		totalScoreLabel = new JLabel("Total Score: 0 points.");
		wordField = new JTextField(10);
		playBtn = new JButton("Submit");
		newHandBtn = new JButton("New Hand");
		
		resultView = new JTextArea(10,10);
		resultView.setLineWrap(true);
		
		JPanel inputArea = new JPanel();
		inputArea.add(playLabel);
		inputArea.add(wordField);		
		JPanel buttonArea = new JPanel();
		buttonArea.add(playBtn);
		buttonArea.add(newHandBtn);
		
		JPanel playArea = new JPanel();
		playArea.setLayout(new GridLayout(2, 1));
		playArea.add(inputArea);
		playArea.add(buttonArea);
		
		JPanel allArea = new JPanel();
		allArea.setLayout(new GridLayout(3, 1));
		allArea.add(playArea);
		allArea.add(totalScoreLabel);
		allArea.add(resultView);
		
		Container c = wordGameFrame.getContentPane();
		c.setLayout(new BorderLayout());
		c.add(allArea, BorderLayout.CENTER);
		c.add(new JLabel("           "), BorderLayout.WEST);
		c.add(new JLabel("           "), BorderLayout.EAST);
		c.add(new JLabel("    "), BorderLayout.NORTH);
		c.add(new JLabel("    "), BorderLayout.SOUTH);
		
		wordGameFrame.pack();
		wordGameFrame.setLocation(250, 250);	
		
		newHandBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				newGame();
			}
		});
		
		playBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				String word = wordField.getText();

				int wordScore = game.tryWord(word);
				if (wordScore == 0)
					resultView.setText("Invalid word, please try again");
				else
					resultView.setText(word+" earned "+wordScore+" points.");

				resultView.append("\n\nCurrent Hand: "+game.getPlayerHand());
				totalScoreLabel.setText("Total Score: "+game.getPlayerPoint()+" points.");

				wordField.setText("");
				System.out.println(hand.handFreq);
			}
		});
	}
	
	private void createChooseSizeFrame() {

		chooseSizeFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
				
		chooseLabel = new JLabel("Choose Hand Size: ");
		sizeField = new JTextField(10);
		submitBtn = new JButton("Submit");
		
		JPanel inputArea = new JPanel();
		inputArea.add(chooseLabel);
		inputArea.add(sizeField);		
		JPanel buttonArea = new JPanel();
		buttonArea.add(submitBtn);
		
		JPanel allArea = new JPanel();
		allArea.setLayout(new GridLayout(2, 1));
		allArea.add(inputArea);
		allArea.add(buttonArea);
		
		Container c = chooseSizeFrame.getContentPane();
		c.setLayout(new BorderLayout());
		c.add(allArea, BorderLayout.CENTER);
		c.add(new JLabel("           "), BorderLayout.WEST);
		c.add(new JLabel("           "), BorderLayout.EAST);
		c.add(new JLabel("    "), BorderLayout.NORTH);
		c.add(new JLabel("    "), BorderLayout.SOUTH);
		
		chooseSizeFrame.pack();
		chooseSizeFrame.setLocation(250, 250);
		chooseSizeFrame.setVisible(true);

		submitBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				size = Integer.parseInt(sizeField.getText());
				chooseSizeFrame.setVisible(false);
				wordGameFrame.setVisible(true);
				newGame();
			}
		});
	}

	private void generateRandomHand(int size) {
		hand = new Hand();//////////////////////////////////////////////////////////////////////////////////////
		Map<Character,Integer> map = new HashMap<Character,Integer>();
		char[] vowels = {'a', 'e', 'i', 'o', 'u'};
		Random rand = new Random();
		int vowelSize = size/4;
		int otherSize = size-vowelSize;
		for (int i = 0; i < vowelSize; i++) {
			char c = vowels[rand.nextInt(5)];
			if (map.get(c) == null)
				map.put(c, 0);
			map.put(c, map.get(c)+1);	
			hand.addLetter(c);
		}
		for (int i = 0; i < otherSize; i++) {
			char c = (char)('a' + rand.nextInt(26));
			if (map.get(c) == null)
				map.put(c, 0);
			map.put(c, map.get(c)+1);
			hand.addLetter(c);
		}
		System.out.println("***"+map);
		//return new Hand(map);
	}
	
	public static void main(String[] args) {
		new WordGameGUI();
	}
}
